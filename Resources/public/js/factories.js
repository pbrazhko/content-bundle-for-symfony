/**
 * Created by pavel on 28.07.14.
 */

'use strict';

angular.module('ContentBundle.factories', [])
    .factory('translator', function () {
        return {
            trans: function (name, domain) {
                return Translator.trans(name, {}, domain);
            }
        }
    })