/**
 * Created by pavel on 26.01.15.
 */

(function ($) {
    $.tree = function (params) {
        return $.tree.impl.init(params);
    };

    $.tree.parameters = {
        'routes': {
            'set_main': '',
            'get_info_file': '',
            'get_info_dir': '',
            'change_status_file': '',
            'change_status_dir': '',
            'delete_file': '',
            'delete_dir': ''
        },
        'info': ''
    };

    $.tree.impl = {
        parameters: {},
        init: function (params) {
            this.parameters = $.extend({}, $.tree.parameters, params);
            this.bind();
        },
        bind: function () {
            var self = this;

            $('.tree li, .tree ol').hover(function () {
                $(this).find('div').css('backgroundColor', '#f5f5f5');
                $(this).find('.action-panel').removeClass('hide');
            }, function () {
                $(this).find('.action-panel').addClass('hide');
                $(this).find('div').css('backgroundColor', '');
            });

            $('.dir div:first').click(function () {
                if ($(this).next('span').hasClass('glyphicon-folder-open')) {
                    $(this).next('span')
                        .removeClass('glyphicon-folder-open')
                        .addClass('glyphicon-folder-close');
                    $(this).closest('ul').next('ul').hide();
                }
                else {
                    $(this).next('span')
                        .removeClass('glyphicon-folder-close')
                        .addClass('glyphicon-folder-open');
                    $(this).closest('ul').next('ul.hide').show();
                }
            });

            $('ol.file .action-panel .delete').click(function (e) {
                e.preventDefault();
                self.delete($(this).closest('ol.file').attr('data-id'), 'file');
            });

            $('ol.dir .action-panel .delete').click(function (e) {
                e.preventDefault();
                self.delete($(this).closest('ol.dir').attr('data-id'), 'dir');
            });

            $('ol.file .action-panel .set-main').click(function (e) {
                e.preventDefault();
                self.setMain($(this).closest('ol.file').attr('data-id'));
            });

            $('ol.file .action-panel .change-status').click(function (e) {
                e.preventDefault();
                if ($(this).find('span').hasClass('glyphicon-check')) {
                    status = true;
                }
                else {
                    status = false;
                }

                self.changeStatus($(this).closest('ol.file').attr('data-id'), 'file', status);
            });

            $('ol.dir .action-panel .change-status').click(function (e) {
                e.preventDefault();
                if ($(this).find('span').hasClass('glyphicon-check')) {
                    status = true;
                }
                else {
                    status = false;
                }

                self.changeStatus($(this).closest('ol.dir').attr('data-id'), 'dir', status);
            });

            $('ol.file').find('div:first').click(function (e) {
                self.getInfo($(this).closest('ol.file').attr('data-id'), 'file');
            });

            $('ol.dir').find('div:first').click(function (e) {
                self.getInfo($(this).closest('ol.dir').attr('data-id'), 'dir');
            });
        },
        delete: function (id, type) {
            callback = function (response) {
                if (response.status == '1') {
                    $('li.' + type + '[data-id="' + id + '"], ol.' + type + '[data-id="' + id + '"]').remove();
                    $('ul[data-parent="' + id + '"], ol[data-parent="' + id + '"]').remove();
                }
                else {
                    alert(response.message);
                }
            };

            route = type == 'file' ? this.parameters.routes.delete_file : this.parameters.routes.delete_dir;

            this.send(route, {'id': id}, callback);
        },
        setMain: function (id) {
            callback = function (response) {
                if (response.status == '1') {
                    $('ol.file').find('div:first > span').each(function (index, span) {
                        $(span).removeClass('glyphicon-home')
                            .removeClass(' warning')
                            .addClass('glyphicon-file');
                    });
                    $('ol.file div.action-panel')
                        .find('span.glyphicon-home')
                        .parent('a').removeClass('hide');

                    $('ol.file[data-id="' + id + '"] div:first span')
                        .removeClass('glyphicon-file')
                        .addClass('glyphicon-home')
                        .addClass(' warning');
                    $('ol.file[data-id="' + id + '"] div.action-panel')
                        .find('span.glyphicon-home')
                        .parent('a').addClass('hide');
                }
                else {
                    alert(response.message);
                }
            };

            this.send(this.parameters.routes.set_main, {'id': id}, callback);
        },
        changeStatus: function (id, type, status) {
            callback = function (response) {
                if (response.status == '1') {
                    var list = $('li.' + type + '[data-id="' + id + '"], ol.' + type + '[data-id="' + id + '"]');
                    if (status == 'false') {
                        $(list).addClass('text-muted');
                        var a = $(list).find('span.glyphicon-unchecked').parent('a');
                        $(list).find('span.glyphicon-unchecked').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                    }
                    else {
                        $(list).removeClass('text-muted');
                        var a = $(list).find('span.glyphicon-check').parent('a');
                        $(list).find('span.glyphicon-check').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                    }
                }
                else {
                    alert(response.message);
                }
            };

            route = type == 'file' ? this.parameters.routes.change_status_file : this.parameters.routes.change_status_dir;

            this.send(route, {'id': id, 'status': status}, callback);
        },

        getInfo: function (id, type) {
            var self = this;
            callback = function (response) {
                if (response.status == '1') {
                    ul = $('<dl/>').addClass('dl-horizontal');

                    if (response.fields && response.fields.length > 0) {
                        for (index in response.fields) {
                            $(ul).append($('<dt/>').text(response.fields[index].title + ':'));
                            $(ul).append($('<dd/>').text(response.fields[index].value));
                        }
                    }
                    $(self.parameters.info).html(ul);
                }
                else {
                    alert(response.message);
                }
            };

            route = type == 'file' ? this.parameters.routes.get_info_file : this.parameters.routes.get_info_dir;

            if (route == null) {
                return;
            }

            this.send(route, {'id': id}, callback);
        },
        send: function (route, data, callback) {
            var self = this;
            $.ajax({
                'type': 'POST',
                'url': self.genereateRoute(route, data),
                'success': callback,
                'dataType': 'json'
            });
        },
        genereateRoute: function (route, data) {
            return Routing.generate(route, data);
        }
    };
})(jQuery);
