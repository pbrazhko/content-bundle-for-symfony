/**
 * Created by pavel on 23.07.14.
 */

'use strict';

angular.module('ContentBundle.directives', [])
    .directive('paginator', function factory($location) {
        return {
            replace: true,
            restrict: 'E',
            controller: function ($scope, paginator) {
                $scope.paginator = paginator;
                $scope.path = $location.path();
            },
            template: '<div class="col-md-6 col-md-offset-0 text-left">' +
            '<div class="pagination pagination-small" style="margin: 0" ng-show="paginator.countPage() > 1">' +
            '<ul>' +
            '<li ng-class="paginator.isFirstPage() && \'disabled\'"><a href="#{[path]}" ng-click="paginator.firstPage()">Start</a></li>' +
            '<li ng-class="paginator.page==page && \'active\'"ng-repeat="page in []|forLoop:this" ><a href="#{[path]}" ng-click="paginator.setPage(page)">{[page+1]}</a></li>' +
            '<li ng-class="paginator.isEndPage() && \'disabled\'"><a href="#{[path]}" ng-click="paginator.endPage()">End</a></li>' +
            '</ul>' +
            '</div></div>'
        };
    })
    .directive('ngHtml', ['$compile', function ($compile) {
        return function (scope, elem, attrs) {
            if (attrs.ngHtml) {
                elem.html(scope.$eval(attrs.ngHtml));
                $compile(elem.contents())(scope);
            }
            scope.$watch(attrs.ngHtml, function (newValue, oldValue) {
                if (newValue && newValue !== oldValue) {
                    elem.html(newValue);
                    $compile(elem.contents())(scope);
                }
            });
        };
    }])
    .directive('ngConfirmClick', [
        function () {
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click', function (event) {
                        if (window.confirm(msg)) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
        }]);