/**
 * Created by pavel on 22.07.14.
 */

'use strict';

angular.module('ContentBundle.controllers', [])
    .controller('indexController', function ($scope, translator, locale, contentStore) {
        $scope.locale = locale();
        $scope.children = {};
        $scope.trans = translator.trans;
        $scope.focus = {};
        $scope.focus_file = {};
        $scope.action_panel = {};
        $scope.action_panel_file = {};

        $scope.getUrl = function (route, params) {
            return Routing.generate(route, params);
        }

        $scope.clearError = function () {
            $scope.error = null;
        };

        $scope.toggleDirectory = function (id) {
            if ($scope.children[id] == undefined || $scope.children[id] == null) {
                $scope.children[id] = 'glyphicon-folder-open';
            }
            else {
                $scope.children[id] = null;
            }
        };

        $scope.changeStatus = function (id, status) {
            contentStore.changeStatus(id, status)
                .success(function (data) {
                    angular.forEach($scope.contents, function (element) {
                        if (element.id == id) {
                            element.is_published = status;
                        }
                    })
                })
                .error(function (data) {
                    $scope.error = data.message;
                })
        };

        $scope.preView = function (id) {
            angular.forEach($scope.contents, function (element) {
                if (element.id == id) {
                    $scope.view = element;
                }
            })
        };

        $scope.deleteContent = function (id) {
            contentStore.deleteContent(id).success(function (data) {
                angular.forEach($scope.contents, function (element) {
                    if (element.id == id) {
                        element.is_deleted = true;
                        element.parent = 0;
                    }
                })
            }).error(function (data) {
                $scope.error = data.message;
            });
        };

        $scope.deleteDirectory = function (id) {
            contentStore.deleteDirectory(id).success(function (data) {
                angular.forEach($scope.directories, function (element) {
                    if (element.id == id || element.parent == id) {
                        element.is_deleted = true;
                        element.parent = 0;
                    }
                })

                angular.forEach($scope.contents, function (element) {
                    if (element.parent == id) {
                        element.is_deleted = true;
                    }
                })
            }).error(function (data) {
                $scope.error = data.message;
            });
        };

        $scope.restoreFile = function (id) {
            contentStore.restoreFile(id).success(function (data) {
                angular.forEach($scope.contents, function (element) {
                    if (element.id == id) {
                        element.is_deleted = false;
                    }
                })
            }).error(function (data) {
                $scope.error = data.message;
            });
        };

        $scope.restoreDirectory = function (id) {
            contentStore.restoreDirectory(id).success(function (data) {
                angular.forEach($scope.directories, function (element) {
                    if (element.id == id || element.parent == id) {
                        element.is_deleted = false;
                    }
                })

                angular.forEach($scope.contents, function (element) {
                    if (element.parent == id) {
                        element.is_deleted = false;
                    }
                })
            }).error(function (data) {
                $scope.error = data.message;
            });
        };

        contentStore.getDirectories().success(function (data) {
            $scope.directories = data;
        }).error(function (data) {
            $scope.error = data.message;
        });

        contentStore.getContents().success(function (data) {
            $scope.contents = data;
        }).error(function (data) {
            $scope.error = data.message;
        });
    })
    .controller('editContent', function ($scope, locale) {
        $scope.locale = locale();

        $scope.isActiveTab = function (code) {
            return $scope.locale == code;
        }

        $scope.context = {
            'ru': document.getElementById('cms_contentbundle_content_content_en').value,
            'en': document.getElementById('cms_contentbundle_content_content_ru').value
        }

        $scope.introduction = {
            'ru': document.getElementById('cms_contentbundle_content_introduction_en').value,
            'en': document.getElementById('cms_contentbundle_content_introduction_ru').value
        }

        $scope.tinymceOptions = {
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_advtab: true,
            height: "200px",
            width: "650px"
        };
    })
    .controller('editDirectories', function ($scope, locale) {
        $scope.locale = locale();

        $scope.isActiveTab = function (code) {
            return $scope.locale == code;
        }

        $scope.tinymceOptions = {
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_advtab: true,
            height: "200px",
            width: "650px"
        };
    });