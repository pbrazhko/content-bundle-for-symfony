/**
 * Created by pavel on 29.10.14.
 */
'use strict';

angular.module('ContentBundle', [
    'ngRoute',
    'ContentBundle.controllers',
    'ContentBundle.directives',
    'ContentBundle.factories',
    'ContentBundle.filters',
    'ContentBundle.services',
    'ui.tinymce'
])
    .config([
        '$interpolateProvider',
        '$routeProvider',
        '$httpProvider',
        function ($interpolateProvider, $routeProvider, $httpProvider) {
            $interpolateProvider.startSymbol('{[').endSymbol(']}');

            tinyMCE.baseURL = '/bundles/core/js/vendor/tinymce';

            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

            // Переопределяем дефолтный transformRequest в $http-сервисе
            $httpProvider.defaults.transformRequest = [function (data) {
                /**
                 * рабочая лошадка; преобразует объект в x-www-form-urlencoded строку.
                 * @param {Object} obj
                 * @return {String}
                 */
                var param = function (obj) {
                    var query = '';
                    var name, value, fullSubName, subValue, innerObj, i;

                    for (name in obj) {
                        value = obj[name];

                        if (value instanceof Array) {
                            for (i = 0; i < value.length; ++i) {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if (value instanceof Object) {
                            for (var subName in value) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if (value !== undefined && value !== null) {
                            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                        }
                    }

                    return query.length ? query.substr(0, query.length - 1) : query;
                };

                return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
            }];
        }
    ])
    .value('locale', function () {
        return document.documentElement.lang
    })