/**
 * Created by pavel on 23.07.14.
 */

'use strict';

angular.module('ContentBundle.services', [])
    .service('contentStore', ['$http', function ($http) {
        return {
            getDirectories: function () {
                return $http.post(Routing.generate('cms_content_ajax_get_directories', {'deleted': true}));
            },
            getDirectoriesList: function () {
                return $http.post(Routing.generate('cms_content_ajax_get_directories'));
            },
            getContents: function () {
                return $http.post(Routing.generate('cms_content_ajax_get_contents', {'deleted': true}));
            },
            changeStatus: function (id, status) {
                return $http.post(Routing.generate('cms_content_ajax_change_status'), {'id': id, 'status': status});
            },
            deleteContent: function (id) {
                return $http.post(Routing.generate('cms_content_ajax_delete'), {'id': id});
            },
            deleteDirectory: function (id) {
                return $http.post(Routing.generate('cms_content_ajax_directories_delete'), {'id': id});
            },
            restoreDirectory: function (id) {
                return $http.post(Routing.generate('cms_content_ajax_directories_restore'), {'id': id});
            },
            restoreFile: function (id) {
                return $http.post(Routing.generate('cms_content_ajax_restore'), {'id': id});
            },
            saveFile: function (data) {
                return $http.post(Routing.generate('cms_content_ajax_save'), data);
            },
            getFile: function (id) {
                return $http.post(Routing.generate('cms_content_ajax_get_contents', {'id': id}));
            },
            saveDirectory: function (data) {
                return $http.post(Routing.generate('cms_content_ajax_directories_save'), data);
            },
            getDirectory: function (id) {
                return $http.get(Routing.generate('cms_content_ajax_get_directories', {'id': id}));
            }
        }
    }])
    .service('userGroupsStore', function ($http) {
        return {
            get: function () {
                return $http.get(Routing.generate('content_get_users_groups'));
            }
        }
    });