<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.05.15
 * Time: 12:57
 */

namespace CMS\ContentBundle\Twig;

use CMS\ContentBundle\Entity\Content;
use CMS\ContentBundle\Services\ContentService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Error_Loader;
use Twig_Source;

class ContentTwigLoader implements \Twig_LoaderInterface, \Twig_ExistsLoaderInterface
{

    /**
     * @var string
     */
    private $locale;

    /**
     * @var ContentService
     */
    private $service;

    private $contents = array();

    public function __construct(ContainerInterface $container)
    {
        $this->service = $container->get('cms.content.content.service');
        $this->locale = $container->getParameter('locale');
    }

    public function getSourceContext($name)
    {
        /** @var Content $content */
        $content = $this->getContent($this->normalizeName($name));

        return new \Twig_Source($this->localeFilter($content->getContent()), $name);
    }

    public function getCacheKey($name)
    {
        return md5($name);
    }

    public function isFresh($name, $time)
    {
        return false;
    }

    /**
     * Check if we have the source code of a template, given its name.
     *
     * @param string $name The name of the template to check if we can load
     *
     * @return bool If the template source code is handled by this loader or not
     */
    public function exists($name)
    {
        if (!$this->validateName($name)) {
            return false;
        }

        $name = $this->normalizeName($name);

        return (null !== $this->getContent($name));
    }

    private function localeFilter($data)
    {
        $locale = strtolower($this->locale);
        if (is_array($data)) {
            foreach ($data as $code => $value) {
                if (strtoupper($code) == strtoupper($locale)) return $value;
            }

            return null;
        }

        return $data;
    }

    private function validateName($name)
    {
        return preg_match('/^content\#(.+)$/i', $name);
    }

    private function normalizeName($name)
    {
        return preg_replace('/^content\#()/i', '', $name);
    }

    /**
     * @param $alias
     * @return Content
     */
    private function getContent($alias)
    {
        if (array_key_exists($alias, $this->contents)) {
            return $this->contents[$alias];
        }

        $content = $this->service->findOneBy(array('alias' => $alias));

        $this->contents[$content->getAlias()] = $content;

        return $content;
    }
}