<?php

namespace CMS\ContentBundle\Controller;

use CMS\ContentBundle\Response\ContentJsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DirectoriesController extends Controller
{
    public function listAction()
    {
        $serviceContent = $this->get('cms.content.content.service');
        $serviceDirectories = $this->get('cms.content.directories.service');

        return $this->render('ContentBundle:Content:list.html.twig', array(
            'contents' => $serviceContent->findAll(),
            'directories' => $serviceDirectories->findAll()
        ));
    }

    public function createAction(Request $request, $parent)
    {
        $service = $this->get('cms.content.directories.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $data->setParent($parent);

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_content_list'));
            }
        }

        return $this->render('ContentBundle:Directories:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.content.directories.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_content_list'));
            }
        }

        return $this->render('ContentBundle:Directories:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function ajaxGetAction(Request $request)
    {
        $id = $request->get('id');
        $service = $this->get('cms.content.directories.service');

        if (null !== $id) {
            $result = $service->findOneById($id);
        } else {
            $result = $service->findAll();
        }

        return new ContentJsonResponse($result);
    }

    public function ajaxDeleteAction(Request $request)
    {
        $id = $request->get('id');
        $service = $this->get('cms.content.directories.service');

        $directory = $service->findOneById($id);

        if (!$directory) {
            return new ErrorJsonResponse(sprintf('Directory of ID \'%s\' not found!', $id));
        }

        try {
            $directory->setIsDeleted(true);
            $service->update($directory);
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'The operation completed successfully'));
    }

    public function ajaxRestoreAction(Request $request)
    {
        $id = $request->get('id');
        $service = $this->get('cms.content.directories.service');

        $directory = $service->findOneById($id);

        if (!$directory) {
            return new ErrorJsonResponse(sprintf('Directory of ID \'%s\' not found!', $id));
        }

        try {
            $directory->setIsDeleted(false);
            $service->update($directory);
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'The operation completed successfully'));
    }

    public function ajaxSaveAction(Request $request)
    {
        $service = $this->get('cms.content.directories.service');

        $id = $request->get('id');
        $title = $request->get('title');

        if (empty($title)) {
            return new ErrorJsonResponse(sprintf('Field "Title" is required!'));
        }

        $newObject = new Directories();

        if (null !== $id) {
            $newObject = $service->findOneById($id);
        }

        $newObject->setTitle($title)
            ->setKeywords($request->get('keywords'))
            ->setDescription($request->get('description'))
            ->setIsPublished($request->get('is_published', false) == 'true' ? true : false)
            ->setParent($request->get('parent', 0))
            ->setIsDeleted(false)
            ->setCreateBy($this->get('security.token_storage')->getToken()->getUser())
            ->setDatecreate(new \DateTime());

        try {
            if (null !== $id) {
                $service->update($newObject);
            } else {
                $service->create($newObject);
            }
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'message' => 'The operation completed successfully'));
    }
}
