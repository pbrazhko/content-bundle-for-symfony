<?php

namespace CMS\ContentBundle\Controller;

use CMS\ContentBundle\Response\ContentJsonResponse;
use Doctrine\DBAL\DBALException;
use Pegas\ContentBundle\Response\ErrorJsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContentController
 * @package CMS\ContentBundle\Controller
 */
class ContentController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $serviceContent = $this->get('cms.content.content.service');
        $serviceDirectories = $this->get('cms.content.directories.service');

        return $this->render('ContentBundle:Content:list.html.twig', array(
            'contents' => $serviceContent->findAll(),
            'directories' => $serviceDirectories->findAll()
        ));
    }

    /**
     * @param Request $request
     * @param $parent
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request, $parent)
    {
        $service = $this->get('cms.content.content.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $data->setParent($parent);
                $data->setCreateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_content_list'));
            }
        }

        return $this->render('ContentBundle:Content:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.content.content.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_content_list'));
            }
        }

        return $this->render('ContentBundle:Content:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @return ContentJsonResponse
     */
    public function ajaxGetAction(Request $request)
    {
        $id = $request->get('id');
        $service = $this->get('cms.content.content.service');

        if (null !== $id) {
            $result = $service->findOneById($id);
        } else {
            $result = $service->findAll();
        }

        return new ContentJsonResponse($result);
    }

    /**
     * @param Request $request
     * @return ContentJsonResponse|ErrorJsonResponse
     */
    public function ajaxChangeStatusAction(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status', false);
        $service = $this->get('cms.content.content.service');

        $content = $service->findOneById($id);

        if (!$content) {
            return new ErrorJsonResponse(sprintf('Content of ID \'%s\' not found!', $id));
        }

        $content->setIsPublished(filter_var($status, FILTER_VALIDATE_BOOLEAN));

        try {
            $service->update($content);
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'message' => 'The operation completed successfully'));
    }

    /**
     * @param Request $request
     * @return ContentJsonResponse|ErrorJsonResponse
     */
    public function ajaxDeleteAction(Request $request)
    {
        $id = $request->get('id');
        $service = $this->get('cms.content.content.service');

        $content = $service->findOneById($id);

        if (!$content) {
            return new ErrorJsonResponse(sprintf('Content of ID \'%s\' not found!', $id));
        }

        try {
            $content->setIsDeleted(true);
            $service->update($content);
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'The operation completed successfully'));
    }

    /**
     * @param Request $request
     * @return ContentJsonResponse|ErrorJsonResponse
     */
    public function ajaxRestoreAction(Request $request)
    {
        $id = $request->get('id');
        $service = $this->get('cms.content.content.service');

        $content = $service->findOneById($id);

        if (!$content) {
            return new ErrorJsonResponse(sprintf('Content of ID \'%s\' not found!', $id));
        }

        try {
            $content->setIsDeleted(false);
            $service->update($content);
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'The operation completed successfully'));
    }

    /**
     * @param Request $request
     * @return ContentJsonResponse|ErrorJsonResponse
     */
    public function ajaxSaveAction(Request $request)
    {
        $service = $this->get('cms.content.content.service');

        $id = $request->get('id');
        $title = $request->get('title');
        $content = $request->get('content');

        if (empty($title)) {
            return new ErrorJsonResponse(sprintf('Field "Title" is required!'));
        }

        if (empty($content)) {
            return new ErrorJsonResponse(sprintf('Field "Content" is required!'));
        }

        $newObject = new \CMS\ContentBundle\Entity\Content();

        if (null !== $id) {
            $newObject = $service->findOneById($id);
        }

        $newObject->setTitle($title)
            ->setKeywords($request->get('keywords'))
            ->setDescription($request->get('description'))
            ->setIntroduction($request->get('introduction'))
            ->setContent($content)
            ->setIsPublished($request->get('is_published', false) == 'true' ? true : false)
            ->setParent($request->get('parent', 0))
            ->setIsDeleted(false)
            ->setCreateBy($this->get('security.token_storage')->getToken()->getUser()->getId())
            ->setDatecreate(new \DateTime());

        try {
            if (null !== $id) {
                $service->update($newObject);
            } else {
                $service->create($newObject);
            }
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'message' => 'The operation completed successfully'));
    }

    public function ajaxSetMainAction(Request $request)
    {
        $id = $request->get('id');

        if (null === $id) {
            return new ErrorJsonResponse(sprintf('Argument \'ID\' is required!'));
        }

        $service = $this->get('cms.content.content.service');

        $currentMain = $service->findOneBy(array('is_main' => true));

        $main = $service->findOneById($id);

        if (!$main) {
            return new ErrorJsonResponse(sprintf('Content of ID \'%s\' not found!', $id));
        }

        try {
            $currentMain->setIsMain(false);
            $service->update($currentMain);

            $main->setIsMain(true);
            $service->update($main);
        } catch (DBALException $e) {
            return new ErrorJsonResponse($e->getMessage());
        }

        return new ContentJsonResponse(array('status' => 'ok', 'message' => 'The operation completed successfully'));
    }
}
