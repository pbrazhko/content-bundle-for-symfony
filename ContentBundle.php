<?php

namespace CMS\ContentBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContentBundle extends Bundle
{
    public function isEnabled()
    {
        return true;
    }

    public function getDescription()
    {
        return array(
            array(
                'title' => 'Content',
                'defaultRoute' => 'cms_content_list'
            )
        );
    }
}
