<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.09.14
 * Time: 16:56
 */

namespace CMS\ContentBundle\Response;

use CMS\ContentBundle\Normalizer\ContentsNormalizer;
use CMS\ContentBundle\Normalizer\DirectoriesNormalizer;
use CMS\ContentBundle\Normalizer\RolesNormalizer;
use CMS\LocalizationBundle\Normalizer\LocalizationNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class ContentJsonResponse extends Response
{
    public function __construct($data, $status = 200, $headers = array())
    {
        $serializer = $this->getSerializer();

        parent::__construct($serializer->serialize($data, 'json'), $status, $headers);
    }

    private function getSerializer()
    {
        return new Serializer(array(
                new DirectoriesNormalizer(),
                new ContentsNormalizer(),
                new RolesNormalizer(),
                new LocalizationNormalizer()
            ),
            array(
                new JsonEncoder()
            )
        );
    }
} 