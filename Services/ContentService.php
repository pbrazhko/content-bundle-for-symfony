<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 26.01.15
 * Time: 15:06
 */
namespace CMS\ContentBundle\Services;


use CMS\ContentBundle\Form\ContentType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormFactory;

class ContentService extends AbstractCoreService
{
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            ContentType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'ContentBundle:Content';
    }
}