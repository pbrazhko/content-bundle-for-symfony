<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.05.15
 * Time: 17:07
 */

namespace CMS\ContentBundle\Form\Types;


use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StylesType extends AbstractType
{

    /**
     * @var string
     */
    private $stylePath;

    public function __construct($stylePath)
    {
        $this->stylePath = $stylePath;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => $this->findStyles(),
            'multiple' => true,
            'required' => false
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    private function findStyles()
    {
        $files = array();

        $finder = new Finder();

        $finder->files()->name('*.css')->in($this->stylePath);

        $finder->sortByName();

        foreach ($finder as $file) {
            $files[$file->getFilename()] = $file->getFilename();
        }

        return $files;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_content_styles_type';
    }

}