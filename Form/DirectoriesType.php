<?php

namespace CMS\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DirectoriesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', 'CMS\LocalizationBundle\Form\Types\LocaleEntityType', array(
                'class' => 'ContentBundle:Directories',
                'property' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false
            ))
            ->add('title', 'CMS\LocalizationBundle\Form\Types\LocaleTextType')
            ->add('alias')
            ->add('keywords', 'CMS\LocalizationBundle\Form\Types\LocaleTextareaType', array(
                'required' => false
            ))
            ->add('description', 'CMS\LocalizationBundle\Form\Types\LocaleTextareaType', array(
                'required' => false
            ))
            ->add('is_published')
            ->add('is_deleted');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\ContentBundle\Entity\Directories',
            'translation_domain' => 'systems'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_contentbundle_directories';
    }
}
