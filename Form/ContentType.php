<?php

namespace CMS\ContentBundle\Form;

use CMS\ContentBundle\Form\Types\StylesType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', LocaleEntityType::class, array(
                'class' => 'ContentBundle:Directories',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false
            ))
            ->add('title', LocaleTextType::class)
            ->add('alias')
            ->add('keywords', LocaleTextareaType::class, array(
                'required' => false
            ))
            ->add('description', LocaleTextareaType::class, array(
                'required' => false
            ))
            ->add('introduction', LocaleTextareaType::class, array(
                'required' => false,
                'attr' => array('class' => 'tinyMce')
            ))
            ->add('content', LocaleTextareaType::class, array(
                'attr' => array('class' => 'tinyMce')
            ))
            ->add('styles', StylesType::class)
            ->add('rating', IntegerType::class)
            ->add('is_main')
            ->add('is_published')
            ->add('is_deleted');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\ContentBundle\Entity\Content',
            'translation_domain' => 'systems'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_contentbundle_content';
    }
}
